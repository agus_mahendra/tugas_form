<?php

    if(isset($_POST["submit"])){
        $nama = $_POST["namakelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Dosen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-sm bg-primary navbar-primary">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="index.php"><img src="https://kompaspedia.kompas.id/wp-content/uploads/2020/08/logo_Universitas-Pendidikan-Ganesha-thumb.png" alt="Logo Undiksha" width="100 px"></a>
</nav>
<div class="container alert alert-light d-flex justify-content-center">
<h1>Sistem Dosen</h1>
</div>
<div class="container justify-content-center">
        <div class="row"> 
            <div class="col text-center">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://image.flaticon.com/icons/png/512/2784/2784488.png" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Data Dosen</h5>
                    <a class="btn btn-primary" href="dosen.php">Form Dosen</a>
                </div>
                </div>
            </div>
            <div class="col text-center">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://image.flaticon.com/icons/png/512/2132/2132333.png" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Kelas</h5>
                    <a class="btn btn-primary" href="kelas.php">Form Kelas</a>
                </div>
                </div>
            </div>
            <div class="col text-center">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://image.flaticon.com/icons/png/512/3652/3652267.png" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Jadwal</h5>
                    <a class="btn btn-primary" href="jadwal.php">Form Jadwal</a>
                </div>
                </div>
            </div>
        </div>   
    </div>
</body>
</html>